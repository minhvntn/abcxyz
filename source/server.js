// Default modules
var express = require('express');
var path = require('path'),
  http = require('http'),

  morgan = require('morgan'),
  bodyParser = require('body-parser'),
  methodOverride = require('method-override');
var routes = require('./routes');
var app = express();

var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;
var db = mongoose.connect('mongodb://localhost/HRM');

var schema = new Schema({
	id: String,
	fullname: String,
	sex: String,
	dob: String,
	email: String,
});

var staff = db.model('staffs', schema);
// Default configuration
app.set('port', process.env.PORT || 7001);
app.set('views', path.join(__dirname, '/views'));
app.set('view engine', 'jade');
app.use(express.favicon(path.join(__dirname, '../public/favicon.ico')));
app.use(express.logger('dev'));
app.use(express.urlencoded());
app.use(express.json());
app.use(express.methodOverride());
app.use(express.cookieParser('frontend'));
app.use(app.router);
app.use(express.static(path.join(__dirname, '../public')));
app.locals.pretty = false;

// Development configuration
if ('development' === app.get('env')) {
  app.use(express.errorHandler({
    dumpExceptions: true,
    showStack: true
  }));
  app.locals.pretty = true;
}

// Routes
app.get('/', routes['index']);
app.get('/index.html', routes['index']);

app.get('/staff', function (req, res) {
	staff.find({}, function (err, data) {
		res.send(data);
	});
});
app.get('/staff/:id', function (req, res) {
	staff.findOne({_id: req.params.id}, function (err, data) {
		res.send(data);
	});
});
app.put('/staff', function (req, res) {
	var edit = {
		id: req.body.id,
		fullname: req.body.fullname,
		dob: req.body.dob,
		sex: req.body.sex,
		email: req.body.email
	};
	staff.update({id: req.body.id}, {$set: edit}, function (error, data) {
		var result = {}
		if (!error) {
			result.success = true;
		} else {
			result.success = false;
		}
		res.send(result);
	});
});
app.post('/login', function (req, res) {
	console.log(req.body);
	var resData = {};
	if (req.body.username === 'admin' && req.body.password === 'admin') {
		resData = ({
			success: true,
			data: {
				username: req.body.username,
				password: req.body.password
			}
		});
	} else {
		resData = ({
			success: false,
			data: 'unauthorized'
		});
	}
	console.log(req.body.username);
	res.send(resData);
});

// Initialization
app.listen(app.get('port'), function() {
  console.log('Template is running on port ' + app.get('port'));
});
