// angular.module('sp.posts.controllers', []).controller('post-ctr', ['$scope', 'postService', function ($scope, postService) {
// 	$scope.getAllPosts = function () {
// 		return postService.getAll();
// 	};
// 	$scope.posts = $scope.getAllPosts();
// }]).controller('postDetails-ctrl', ['$stateParams', '$state', '$scope', 'postService', function ($stateParams, $state, $scope, postService) {
// 	$scope.getPostById = function (id) {
// 		return postService.getPostById(id);
// 	};
// 	$scope.closePost = function() {
// 		$state.go('allPosts');
// 	};
// 	$scope.singlePost = $scope.getPostById($stateParams.id);
// }]);
app.controller('post-ctrl', function ($scope, $routeParams, $http, postService) {
  $scope.post = postService;
 	$scope.getAllPosts = function () {
 		return postService.getAll();
 	}
 	$scope.posts = $scope.getAllPosts();
});

app.controller('postDetails-ctrl', function ($scope, $location, $routeParams, postService) {
	$scope.getPostById = function (id) {
		return postService.getPostById(id);
	};
	$scope.closePost = function () {
		$location.path('/post');
	};
	$scope.singlePost = $scope.getPostById($routeParams.id);
});

app.controller('page-ctrl', function ($scope, $location, Page) {
$scope.contact = Page;
});
app.controller('contact-ctrl', function ($scope, $location, Contact) {
$scope.contact = Contact;
$scope.asd = "asdad";
});
app.controller('about-ctrl', function ($scope, $location, About) {
$scope.about = About;
});

app.controller('gallery-ctrl', function ($scope, Gallery) {
	$scope.index = 0;
	$scope.getAllPhotos = function () {
		return Gallery.getAllPhotos();
	}
	$scope.photos = $scope.getAllPhotos();

	$scope.isActive = function (index) {
		return $scope.index === index;
	}

	$scope.next = function () {
		$scope.index = ($scope.index > 0) ? --$scope.index : $scope.photos.length -1;
	}
	$scope.prev = function () {
		$scope.index = ($scope.index < $scope.photos.length - 1) ? ++$scope.index : 0;
	}
});

app.controller('login-ctrl', ['$scope', '$location', 'authService', 'api', function ($scope, $location, authService, api) {
  $scope.buttonText = "Login";
  $scope.login = function () {
    $scope.buttonText = "Logging in ....";
    authService.login($scope.username, $scope.password).then(function (data) {
      $location.path('/post');
    });
  }
}]);

app.controller('staff-ctrl', ['$scope', '$location', 'staffService', 'staffs', function ($scope, $location, staffService, staffs) {
	$scope.staffs = staffs;
	$scope.detail = null;
	$scope.edited = false;
	$scope.viewDetail = function (id) {
		staffService.getStaff(id).then(function (data) {
			$scope.detail = data;
			console.log(data);
		});
	}
	$scope.edit = function () {
		$scope.edited = true;
	}
}]);