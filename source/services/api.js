app.factory('api', ['$http', '$q', '$resource', function ($http, $q, $resource) {
	return {
		rest: function (url, params, actions, options) {
			return $resource(url, params, actions, options);
		},
		call: function (url, type, params) {
			var deferred = $q.defer();
			$http({
				url: url || '',
				method: type || 'get',
				data: params || {}
			})
			.success(function (data){
				deferred.resolve(data);
				console.log(data)
			})
			.error(function (response) {
				deferred.reject(response);
			});	
			return deferred.promise;
		}
	}
}]);