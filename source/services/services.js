app.factory('Page', function ($resource) {
  return {
    "text": "Form"
  }
});
app.factory('Contact', function ($resource) {
  return {
    "text": "Contact"
  }
});
app.factory('postService', function ($resource) {
	return {
		posts: [{
			id: 1,
			title: 'Tab 1',
			content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
			permalink: 'simple-title1',
			author: 'Sandeep',
			datePublished: '2012-04-04'	
		}, {
			id: 2,
			title: 'Tab 2',
			content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
			permalink: 'simple-title2',
			author: 'Sandeep',
			datePublished: '2012-05-04'
		}, {
			id: 3,
			title: 'Tab 3',
			content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
			permalink: 'simple-title3',
			author: 'Sandeep',
			datePublished: '2012-06-04'
		}],
		getAll: function () {
			return this.posts;
		},
		getPostById: function (id) {
			for (var i in this.posts) {
				if (this.posts[i].id == id) {
					return this.posts[id - 1];
				}
			}
		},
	}
});
app.factory('Gallery', function ($resource) {
  return {
    photos: [
	    {src: 'http://farm9.staticflickr.com/8042/7918423710_e6dd168d7c_b.jpg', desc: 'Image 01' },
	    {src: 'http://farm9.staticflickr.com/8449/7918424278_4835c85e7a_b.jpg', desc: 'Image 02' },
	    {src: 'http://farm9.staticflickr.com/8457/7918424412_bb641455c7_b.jpg', desc: 'Image 03' },
	    {src: 'http://farm9.staticflickr.com/8179/7918424842_c79f7e345c_b.jpg', desc: 'Image 04' },
	    {src: 'http://farm9.staticflickr.com/8315/7918425138_b739f0df53_b.jpg', desc: 'Image 05' },
	    {src: 'http://farm9.staticflickr.com/8461/7918425364_fe6753aa75_b.jpg', desc: 'Image 06' },
    ],

    getAllPhotos: function () {
    	return this.photos;
    }
  }
});

app.factory('authService', ['$rootScope', '$resource', 'api', '$cookieStore',
	function ($rootScope, $resource, api, $cookieStore) {
		var auth = {};
		auth.login = function (username, password) {
			return api.call('/login', 'POST', {username:username, password:password}).then(function (res) {
				if (res.success) {
					auth.user = res.data;
					$rootScope.globals.currentUser = auth.user;
					$cookieStore.put('globals', $rootScope.globals);
				}
			});
		}
		return auth;
}]);
// app.run(['$rootScope', '$cookieStore', 'authService', function ($rootScope, $cookieStore, authService) {
// 	$rootScope.$on('$stateChangeError', function () {
// 		window.alert(1);
// 	});
// }]);
app.factory('staffService', ['$rootScope', '$resource', 'api', function ($rootScope, $resource, api) {
	var staff = {};
	var staffService = $resource('/staff/:id',  {id:'@id'}, { update: { method: 'PUT' }});
	staff.getAll =  function () {
		return staffService.query().$promise;
	}
	staff.getStaff = function (id) {
		return staffService.get({id: id}).$promise;
	}
	staff.updateStaff = function (staff, cb) {
		console.log(staff, cb);
		this.getStaff(staff.id).then(function (edit) {
			edit.id = staff.id;
			edit.firstname = staff.firstname;
			edit.dob = staff.dob;
			edit.sex = staff.sex;
			edit.email = staff.email;
			return edit.$update(function (data) {
				cb(data);
			});
		});
	}
	return staff;
}]);
