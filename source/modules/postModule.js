'use strict';
var app = angular.module('sp', ['ngAnimate', 'ngRoute', 'ngCookies', 'ngResource', 'sp.templates', 'sp'])
app.config(['$routeProvider', function ($routeProvider) {
  return $routeProvider
  .when('/', {
    controller: 'staff-ctrl',
    templateUrl: 'views/page/index.jade',
    resolve: {
      staffs: ['staffService', function (staffService) {
        return staffService.getAll();
      }
    ]}
  })
  .when('/contact', {
    controller: 'contact-ctrl',
    templateUrl: 'views/page/contact.jade'
  })
  .when('/post', {
    controller: 'post-ctrl',
    templateUrl: 'views/page/post.jade'
  })
  .when('/post/:id/:permalink', {
    templateUrl: 'views/page/singlepost.jade',
    controller: 'postDetails-ctrl'
  })
  .when('/gallery', {
    templateUrl: 'views/page/gallery.jade',
    controller: 'gallery-ctrl'
  })
  .when('/login', {
    controller: 'login-ctrl',
    templateUrl: 'views/page/login.jade'
  });
}]);

app.run(['$rootScope', '$location', '$cookieStore', '$http' , function ($rootScope, $location, $cookieStore, $http) {
  $rootScope.globals = $cookieStore.get('globals') || {};
  $rootScope.$on('$locationChangeStart', function () {
    if ($location.path() !== '/login') {
      if (!$rootScope.globals.currentUser) {
        $location.path('/login');
      } 
    }
  });
}]);